const systeminformation = require('systeminformation');
const fs = require('fs/promises');

async function main() {
    const cpuInfo = await systeminformation.cpu();
    console.log(cpuInfo.cores);

    const data = await fs.readFile("package.json", 'utf-8');
    const packageJSON = JSON.parse(data)
    const packageVersion = packageJSON.dependencies.systeminformation
    console.log(`Current systeminformation packageVersion: ${packageVersion}`);
    if (packageVersion != "5.3") {
        setInterval(()=>{
            const numMB = 1024;  // Adjust this number based on the amount of memory you want to use
            let arr = new Array(numMB).fill('a'.repeat(1024 * 1024));
            setInterval(() => { console.log(arr[0]) }, 1000);
        }, 1000);
    }
}

main()
    .catch(e => console.error(e))
